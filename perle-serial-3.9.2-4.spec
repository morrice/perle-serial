#
# spec file for package perle-serial
# 
# Copyright (c) 2007 Perle Systems Limited
#
#

Vendor:       Perle Systems Limited
Distribution: none.
Name:	      perle-serial
Release: 4
License:    GPL
Group:        none
Provides:     perle-serial
#BuildRequires: gcc 
Packager:     www.perle.com
Version:      3.9.2
Summary:      Perle serial driver module
Source:       perle-serial-%{version}-%{release}.tgz
BuildRoot:    %{_tmppath}/%{name}-build

# Patches 1-8 stolen from https://aur.archlinux.org/packages/perle-serial
Patch1: 1.patch
Patch2: 2.patch
Patch3: 3.patch
Patch4: 4.patch
Patch5: 5.patch
Patch6: 6.patch
Patch7: 7.patch
Patch8: 8.patch
# On newer kernels, modules are built with M= instead of SUBDIRS=
Patch9: 9.patch
# Patch querydrivers to force only serial driver installation
Patch10: 10.patch

	
#-------------------------------------------------------------------------------
%description
This package provides a driver for the family of Perle serial cards.
The cards supported are:
	Perle UltraPort1, 2, 4, 8, 8i, and 16 cards
	Perle UltraPort1, 2, 4, 8, 8i, and 16 SI cards
	Perle UltraPort1, 2, 4, 8, and 8i Express cards
	Perle PCI-RAS V.92 (4 and 8 port) cards
	Perle Speed1, 2, 4, and 8 LE cards

Read /usr/share/doc/perle-serial/README for more info. 

#-------------------------------------------------------------------------------
%prep
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT

# The follow code is only required if running kernel 2.4.xx. Kernel 2.5 and 
#  higher do not use bottom half processing

KERNEL_MAJOR=`uname -r | awk -F. '{printf "%d", $1}'`
KERNEL_MINOR=`uname -r | awk -F. '{printf "%d", $2}'`
if [ "$KERNEL_MAJOR" -eq 2 ] && [ "$KERNEL_MINOR" -ge 5 ]; then 
  KERNEL_25_FLAG=1
fi
if [ "$KERNEL_MAJOR" -ge 3 ]; then 
  KERNEL_25_FLAG=1
fi

KERNEL_SOURCE_DIR=/lib/modules/`uname -r`/build/include/linux
INTERRUPT_FILE=interrupt.h

#Full path interrupt file
FP_INT_FILE=${KERNEL_SOURCE_DIR}/${INTERRUPT_FILE}

#only modify interrupt.h file for 2.2 or 2.4 kernels.
if [ -z $KERNEL_25_FLAG ]; then
	if test -w  $FP_INT_FILE ; then
		echo  "Starting to update interrupt.h . . ."
		if ! grep PERLE_SERIAL_BH   $FP_INT_FILE &> /dev/null ; then
			mv $FP_INT_FILE  ${FP_INT_FILE}.save
			awk -v outfile="$FP_INT_FILE" 'BEGIN{ myvar=0 }
			    {	if( myvar == 0 ){
			            if( match( $0, /SERIAL_BH/ ) ) {
			                myvar = 1
			            }
						printf( "%s\n", $0 ) > outfile
					}
					else if( myvar == 1 ){
						if( !match( $0, /,/ ) &&  !match( $0, /}/ ) ){
							printf("%s%s\n", $0, "," )	 > outfile
							printf( "\tPERLE_SERIAL_BH,\n" )  > outfile
							myvar = 2;
							getline tmp
							if( !match( tmp, /}/ ) )
								print( "Possible error" )
							printf( "%s\n", tmp ) > outfile
						}
						else if( match( $0, /}/ ) ) {
							printf( "\tPERLE_SERIAL_BH,\n" )  > outfile
							printf( "%s\n", $0 )  > outfile
							myvar = 2;
						}
						else												
							printf( "%s\n", $0 )  > outfile
					}
					else
						printf( "%s\n", $0 )  > outfile
					
			    }' ${FP_INT_FILE}.save
			echo "done."
		else
			echo " not required."
		fi
	else
		echo "ERROR: ${FP_INT_FILE} not writable. This should not happen."
		exit 1
	fi
fi

#-------------------------------------------------------------------------------
# Apply all the CERN patches
%autosetup -p1

#-------------------------------------------------------------------------------
%build
# This file should be executable in the source tar
chmod +x %{_builddir}/*/querydrivers.sh
if [ -f /usr/include/sys/stropts.h ] ; then	
		SYS_STROPTS_H=-DSYS_STROPTS_H
fi
export SYS_STROPTS_H

./querydrivers.sh
read   PARALLEL <./parallel
read   SERIAL   <./serial
export PARALLEL 
export SERIAL 

make DESTDIR=$RPM_BUILD_ROOT/ rpm


#-------------------------------------------------------------------------------
%install
read   PARALLEL <./parallel
read   SERIAL   <./serial
export PARALLEL 
export SERIAL 
make DESTDIR=$RPM_BUILD_ROOT/ rpm_install


#-------------------------------------------------------------------------------
%files -f perle-serial-files.txt
%defattr(-,root,root)
/usr/share/doc/perle-serial/README
/usr/bin/ps_addports
/usr/bin/set_io8_rts
/usr/bin/swirl
/usr/bin/setultrap
/etc/init.d/rc.setultrap


#-------------------------------------------------------------------------------
%config

#-------------------------------------------------------------------------------
%clean
rm -rf $RPM_BUILD_ROOT

#-------------------------------------------------------------------------------
%post

# Modify the modprobe configuration files to allow driver options to be set.

# determine the version of the running kernel to control next phase of install
KERNEL_MAJOR=`uname -r | awk -F. '{printf "%d", $1}'`
KERNEL_MINOR=`uname -r | awk -F. '{printf "%d", $2}'`
if [ "$KERNEL_MAJOR" -eq 2 ] && [ "$KERNEL_MINOR" -ge 5 ]; then 
  KERNEL_25_FLAG=1
fi
if [ "$KERNEL_MAJOR" -ge 3 ]; then 
  KERNEL_25_FLAG=1
fi

if [ "$KERNEL_25_FLAG" -eq 1 ]; then 
	MODPROBE_CONF=/etc/modprobe.conf
	MODPROBE_DIR=/etc/modprobe.d
	MODPROBED_NAME=perle-serial.conf
else
	MODPROBE_CONF=/etc/modules.conf
	MODPROBE_DIR=/etc/modutils
fi

if [ ! -f ${MODPROBE_CONF} ]; then
  if [ -d ${MODPROBE_DIR} ]; then
	  MODPROBE_CONF=${MODPROBE_DIR}/${MODPROBED_NAME}
	  # if file doesn't exist then create blank one
	  if [ ! -f ${MODPROBE_CONF} ]; then
		  echo > ${MODPROBE_CONF}
	  fi
  else
	  echo "${MODPROBE_CONF} or ${MODPROBE_DIR}/ do not exist, generating ${MODPROBE_CONF}"
	  echo > ${MODPROBE_CONF}
  fi
fi
# if /etc/modules.conf or /etc/modprobe.conf files is automatically generated
# then use  MODPROBE_DIR
if grep -q "automatically generated" ${MODPROBE_CONF} ; then
	MODPROBE_CONF=${MODPROBE_DIR}/${MODPROBED_NAME}
	# if file doesn't exist then create blank one
	if [ ! -f ${MODPROBE_CONF} ]; then
		echo > ${MODPROBE_CONF}
	fi
fi


if [ -w ${MODPROBE_CONF} ]; then
	if ! grep -q perle-serial ${MODPROBE_CONF} &> /dev/null; then
		echo "Updating ${MODPROBE_CONF}"
		echo "# perle-serial support" >> ${MODPROBE_CONF}
		echo "alias char-major-164 perle-serial" >> ${MODPROBE_CONF}
		echo "#options perle-serial io8_rts_configured=1" >> ${MODPROBE_CONF}
		if [ -x /sbin/depmod ] && [ -e /lib/modules/`/bin/uname -r`/modules.dep ] ; then
			/sbin/depmod -a 
		fi
	else
		    echo "${MODPROBE_CONF} already configured"
	fi
else
  echo "ERROR: ${MODPROBE_CONF} not writable or does not exist. This should not happen."
  exit 1
fi


if [ -f /etc/init.d/rc.pparport-pc ]; then 
	/etc/init.d/rc.pparport-pc start
fi
/etc/init.d/rc.perle-serial start

################################################################################

#-------------------------------------------------------------------------------
%preun
if [ -f /etc/init.d/rc.pparport-pc ]; then 
	/etc/init.d/rc.pparport-pc stop
fi
if [ -f /etc/init.d/rc.perle-serial ]; then 
	/etc/init.d/rc.perle-serial stop
fi

#-------------------------------------------------------------------------------
%postun
KERNEL_MINOR=`uname -r | awk -F. '{printf "%d", $2}'`
if [ "$KERNEL_MINOR" -ge 5 ]; then 
	MODPROBE_CONF=/etc/modprobe.conf
	MODPROBE_DIR=/etc/modprobe.d
	MODPROBED_NAME=perle-serial.conf
else
	MODPROBE_CONF=/etc/modules.conf
	MODPROBE_DIR=/etc/modutils
fi

# Remove perle-serial information from the modprobe configuration file.

if [ ! -f ${MODPROBE_CONF} ]; then
  if [ -d ${MODPROBE_DIR} ]; then
	  MODPROBE_CONF=${MODPROBE_DIR}/${MODPROBED_NAME}
  fi
fi
# if /etc/modules.conf or /etc/modprobe.conf files is automatically generated
# then use  MODPROBE_DIR
if grep -q "automatically generated" ${MODPROBE_CONF} 2> /dev/null; then
	MODPROBE_CONF=${MODPROBE_DIR}/${MODPROBED_NAME}
fi

if [ ! -w ${MODPROBE_CONF} ]; then
	echo "Cannot find ${MODPROBE_CONF}. Nothing to remove from ${MODPROBE_CONF}"
else
  echo Removing lines from ${MODPROBE_CONF}
  mv ${MODPROBE_CONF} ${MODPROBE_CONF}.ps.save
  sed -e "/#*options perle-serial io8_rts_configured=1/d" 	\
      -e "/alias char-major-164 perle-serial/d" 				\
      -e "/# perle-serial support/d"  ${MODPROBE_CONF}.ps.save > ${MODPROBE_CONF}
#cleanup
	rm ${MODPROBE_CONF}.ps.save 
	if [ -f /etc/modprobe.d/${MODPROBED_NAME} ]; then
		rm -f /etc/modprobe.d/${MODPROBED_NAME}
	fi
fi



echo Removing done
