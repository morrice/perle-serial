# perle-serial

Attempt at getting https://www.perle.com/downloads/mp_speedle.shtml to build on a recent (5.x / el9) kernel


```
morrice@morricedesktop perle-serial]$ sudo rpm -ivh /home/morrice/perle-serial/build/RPMS/x86_64/perle-serial-3.9.2-4.x86_64.rpm
[sudo] password for morrice: 
Verifying...                          ################################# [100%]
Preparing...                          ################################# [100%]
Updating / installing...
   1:perle-serial-3.9.2-4             ################################# [100%]
/etc/modprobe.d/perle-serial.conf already configured
Starting Perle-Serial driver
[morrice@morricedesktop perle-serial]$ modinfo perle-serial
filename:       /lib/modules/5.14.0-362.8.1.el9_3.x86_64/misc/perle-serial.ko
license:        GPL
author:         Perle Systems Limited <www.perle.com>
description:    Perle Serial Driver
rhelversion:    9.3
srcversion:     A4749BC4319BC7DD4DE4AD3
alias:          pci:v0000155Fd0000B034sv00001415sd00009505bc*sc*i*
alias:          pci:v0000155Fd0000B033sv00001415sd00009505bc*sc*i*
alias:          pci:v0000155Fd0000B032sv00001415sd00009505bc*sc*i*
alias:          pci:v0000155Fd0000B031sv00001415sd00009505bc*sc*i*
alias:          pci:v0000155Fd0000B030sv00001415sd00009505bc*sc*i*
alias:          pci:v0000155Fd00000351sv00001415sd00009511bc*sc*i*
alias:          pci:v0000155Fd00000351sv00001415sd00009501bc*sc*i*
alias:          pci:v0000155Fd00000321sv00001415sd00009511bc*sc*i*
alias:          pci:v0000155Fd00000321sv00001415sd00009501bc*sc*i*
alias:          pci:v0000155Fd00000331sv00001415sd00009501bc*sc*i*
alias:          pci:v0000155Fd00000311sv00001415sd00009521bc*sc*i*
alias:          pci:v0000155Fd00000361sv00001415sd00009521bc*sc*i*
alias:          pci:v0000155Fd0000B026sv00001415sd00009505bc*sc*i*
alias:          pci:v0000155Fd0000B022sv00001415sd00009505bc*sc*i*
alias:          pci:v0000155Fd0000B021sv00001415sd00009505bc*sc*i*
alias:          pci:v0000155Fd0000B014sv00001415sd00009505bc*sc*i*
alias:          pci:v0000155Fd0000B013sv00001415sd00009505bc*sc*i*
alias:          pci:v0000155Fd0000B012sv00001415sd00009505bc*sc*i*
alias:          pci:v0000155Fd0000B011sv00001415sd00009505bc*sc*i*
alias:          pci:v0000155Fd0000B008sv00001415sd00009511bc*sc*i*
alias:          pci:v0000155Fd0000B008sv00001415sd00009501bc*sc*i*
alias:          pci:v0000155Fd0000B004sv00001415sd00009501bc*sc*i*
alias:          pci:v0000155Fd0000B002sv00001415sd00009521bc*sc*i*
alias:          pci:v0000155Fd0000B001sv00001415sd00009521bc*sc*i*
alias:          pci:v0000155Fd00000251sv000013A8sd00000158bc*sc*i*
alias:          pci:v0000155Fd00000221sv000013A8sd00000158bc*sc*i*
alias:          pci:v0000155Fd00000231sv000013A8sd00000154bc*sc*i*
alias:          pci:v0000155Fd00000211sv000013A8sd00000152bc*sc*i*
alias:          pci:v0000155Fd00000261sv000013A8sd00000152bc*sc*i*
alias:          pci:v000013A8d00000158sv0000155Fsd00000251bc*sc*i*
alias:          pci:v000010B5d00009030sv0000155Fsd00000241bc*sc*i*
alias:          pci:v000013A8d00000158sv0000155Fsd00000221bc*sc*i*
alias:          pci:v000013A8d00000154sv0000155Fsd00000231bc*sc*i*
alias:          pci:v000013A8d00000152sv0000155Fsd00000211bc*sc*i*
alias:          pci:v000013A8d00000152sv0000155Fsd00000261bc*sc*i*
alias:          pci:v000010B5d00009030sv0000155Fsd00000051bc*sc*i*
alias:          pci:v000010B5d00009030sv0000155Fsd00000041bc*sc*i*
alias:          pci:v000010B5d00009030sv0000155Fsd00000021bc*sc*i*
alias:          pci:v000010B5d00009030sv0000155Fsd00000031bc*sc*i*
alias:          pci:v000010B5d00009030sv0000155Fsd00000011bc*sc*i*
alias:          pci:v000010B5d00009030sv0000155Fsd00000061bc*sc*i*
alias:          pci:v000010B5d00009030sv0000155Fsd0000F010bc*sc*i*
alias:          pci:v000010B5d00009030sv0000155Fsd0000F001bc*sc*i*
depends:        
retpoline:      Y
name:           perle_serial
vermagic:       5.14.0-362.8.1.el9_3.x86_64 SMP preempt mod_unload modversions 
parm:           io8_rts_configured:0=DTR, 1=RTS mode on 8i cards (int)
[morrice@morricedesktop perle-serial]$ 
```
